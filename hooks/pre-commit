#!/bin/bash

set -e

echo "performing pre-commit checks"
 # Lint Markdown documents.
git ls-files -z '*.md' | xargs -0 mdl --style .mdl.rb

# C++ lint, build and test is slow, so we only run these checks if the C++
# source files have been staged.
NEEDS_TESTING=0
for FILE in $(git diff --cached --name-only | grep -E '\.(cpp|hpp|h|cmake)$')
    do
        NEEDS_TESTING=1
        echo "${FILE} has changed since the last commit - hyper-cube-fastpack will now be tested."
        break
    done
if [[ ${NEEDS_TESTING} -ne 0 ]];
then
    # Format C++ code.
    NEEDS_FORMATTING=0
    for FILE in hyper-cube-fastpack/FFD-Packer/src/*.{cpp,hpp,h}
    do
        if [[ $(clang-format --style=WebKit --output-replacements-xml ${FILE} | grep "replacement offset=") ]]
        then
            echo "${FILE} needs reformatting with clang-format."
            NEEDS_FORMATTING=1
        fi
    done
    if [[ ${NEEDS_FORMATTING} -ne 0 ]]
    then
        exit 1
    fi

    # Test C++ code.
    cd FFD-Packer/test
    rm -rf build
    mkdir build
    cd build
    cmake ..
    make
    cp ../../../data/*.txt .
    ./Test
    rm *.txt

    cd ../../src

    # Run formatting lint on CPP code.
    clang-tidy *.hpp *.cpp --

    if [ $? -ne 0 ]; then
        exit 1
    fi
fi

// Copyright (c) 2020-2021 FFD-Packer contributors <https://gitlab.com/jakirk1/FFD-Packer>
// This file is part of the FFD-Packer project, released under the BSD 3-Clause License.

#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this in one cpp file
#include "../src/Node.hpp"
#include "../src/Node.cpp"
#include "../src/BinPacker.hpp"
#include "../src/ShapeSorter.hpp"
#include "../src/ShapeSorter.cpp"
#include "../src/RISSPacker.hpp"
#include "../src/RISSPacker.cpp"
#include "catch.hpp"
#include <algorithm>

TEST_CASE("Unit tests for ShapeSorter", "FFD-Packer test")
{
    auto lambda = [](const BinItem& lhs, const BinItem& rhs) { return lhs.dims[1] > rhs.dims[1]; };
    SECTION("Exception thrown for incorrect number of rectangle input dimensions")
    {
        auto loader = ShapeSorter("3Dims.txt");
        CHECK_THROWS(loader.load(2));
    }
    SECTION("Exception thrown for negative X rectangle input dimension")
    {
        auto loader = ShapeSorter("negDimX.txt");
        CHECK_THROWS(loader.load(2));
    }

    SECTION("Exception thrown for negative X rectangle input dimension")
    {
        auto loader = ShapeSorter("negDimY.txt");
        CHECK_THROWS(loader.load(2));
    }
    SECTION("Unit test for rect_sorter()")
    {
        auto loader = ShapeSorter("uniform1600.txt");
        loader.load(2);
        std::sort(loader.rects.begin(), loader.rects.end(), lambda);

        CHECK(loader.rects[0].dims[1] == Approx(0.99932));
        CHECK(loader.rects[1].dims[1] == Approx(0.99914));
    }
}

TEST_CASE("Unit tests BinPacker", "FFD-Packer test")
{
    const auto BIN_DIM = 1.0;
    auto packer = BinPacker<BinItem, BinItem>(BIN_DIM);
    std::vector<BinItem> rects;
    auto rect = BinItem();
    rect.dims.push_back(0.98);
    rect.dims.push_back(0.98);
    rects.push_back(rect);

    rect.dims[0] = 0.95;
    rect.dims[1] = 0.95;
    rects.push_back(rect);

    auto shelfPacker = BinPacker<BinItem, Bin>(BIN_DIM);

    SECTION("Unit tests of BinPacker methods, pack(), extractTree(), and add()")
    {
        packer.pack(rects);
        CHECK(packer.bins.size() == 2);

        auto gap = BIN_DIM;
        for (const auto& rect : packer.bins[0].items) {
            gap -= rect.dims[0];
        }
        CHECK(gap == Approx(0.02));

        gap = BIN_DIM;
        for (const auto& rect : packer.bins[1].items) {
            gap -= rect.dims[0];
        }
        CHECK(gap == Approx(0.05));

        for (auto& shelf : packer.bins) {
            shelf.dims.push_back(shelf.items[0].dims[1]);
        }

        shelfPacker.pack(packer.bins);
        CHECK(shelfPacker.bins.size() == 2);
    }

    SECTION("Unit test of BinPacker method extractTree()")
    {
        auto rect = BinItem();
        rect.dims.push_back(0.97);
        rect.dims.push_back(0.97);
        rects.push_back(rect);

        rect.dims[0] = 0.025;
        rect.dims[1] = 0.025;
        rects.push_back(rect);

        packer.pack(rects);

        auto gap = BIN_DIM;
        for (const auto& rect : packer.bins[2].items) {
            gap -= rect.dims[0];
        }

        CHECK(packer.bins.size() == 3);
        CHECK(gap == Approx(0.005));

        for (auto& shelf : packer.bins) {
            shelf.dims.push_back(shelf.items[0].dims[1]);
        }

        shelfPacker.pack(packer.bins);
        CHECK(shelfPacker.bins.size() == 3);
    }

    SECTION("Unit test of BinPacker method redBlackConstraint()")
    {

        auto rect = BinItem();
        rect.dims.push_back(0.99);
        rect.dims.push_back(0.99);
        rects.push_back(rect);

        rect.dims[0] = 0.95;
        rect.dims[1] = 0.95;
        rects.push_back(rect);
        rects.push_back(rect);

        rect.dims[0] = 0.005;
        rect.dims[1] = 0.005;
        rects.push_back(rect);

        packer.pack(rects);

        auto gap = BIN_DIM;
        for (const auto& rect : packer.bins[2].items) {
            gap -= rect.dims[0];
        }
        CHECK(gap == Approx(0.005));

        CHECK(packer.bins.size() == 5);

        for (auto& shelf : packer.bins) {
            shelf.dims.push_back(shelf.items[0].dims[1]);
        }
        shelfPacker.pack(packer.bins);
        CHECK(shelfPacker.bins.size() == 5);
    }

    SECTION("Unit test of BinPacker method updateTree()")
    {
        auto rect = BinItem();
        rect.dims.push_back(0.99);
        rect.dims.push_back(0.99);
        rects.push_back(rect);

        rect.dims[0] = 0.95;
        rect.dims[1] = 0.95;
        rects.push_back(rect);
        rects.push_back(rect);

        rect.dims[0] = 0.045;
        rect.dims[1] = 0.01;
        rects.push_back(rect);
        rects.push_back(rect);
        rects.push_back(rect);

        packer.pack(rects);

        auto gap = BIN_DIM;
        for (const auto& rect : packer.bins[1].items) {
            gap -= rect.dims[0];
        }
        CHECK(gap == Approx(0.005));

        gap = BIN_DIM;
        for (const auto& rect : packer.bins[3].items) {
            gap -= rect.dims[0];
        }
        CHECK(gap == Approx(0.005));

        gap = BIN_DIM;
        for (const auto& rect : packer.bins[4].items) {
            gap -= rect.dims[0];
        }
        CHECK(gap == Approx(0.005));

        CHECK(packer.bins.size() == 5);
        for (auto& shelf : packer.bins) {
            shelf.dims.push_back(shelf.items[0].dims[1]);
        }

        shelfPacker.pack(packer.bins);
        CHECK(shelfPacker.bins.size() == 5);
    }
}

TEST_CASE("Integration tests", "FFD-Packer test")
{
    auto lambda = [](const BinItem& lhs, const BinItem& rhs) { return lhs.dims[1] > rhs.dims[1]; };
    const auto maxDim = 0.99999;
    const auto MIN_LAYERS_PER_BIN = 4.0;

    SECTION("Integration test a: 20000 uniformly distributed rects")
    {
        const auto* fileName = "uniform20000.txt";
        auto loader = ShapeSorter(fileName);
        loader.load(2);
        std::sort(loader.rects.begin(), loader.rects.end(), lambda);

        const auto totalArea = 4999.064;

        CHECK(maxDim == Approx(loader.getMaxDim()));
        CHECK(totalArea == Approx(loader.getTotalArea()));

        auto packer = BinPacker<BinItem, BinItem>(loader.getMaxDim() * MIN_LAYERS_PER_BIN);
        packer.pack(loader.rects);
        CHECK(packer.bins.size() == 2502);
    }

    SECTION("Integration test b: 20000 uniformly distributed rects")
    {

        RISSPacker pack = RISSPacker("uniform20000.txt", "result.txt");
        pack.run();

        std::string line;
        std::ifstream infile("result.txt");

        std::string x;
        float y = 0.0;

        std::getline(infile, line);
        std::istringstream iss(line);

        iss >> x >> y;
        CHECK(x == "SideLength=");
        CHECK(y == Approx(71.9993));
    }

    SECTION("Integration test a: 160000 uniformly distributed rects")
    {
        const auto* fileName = "uniform160000.txt";
        auto loader = ShapeSorter(fileName);
        loader.load(2);
        std::sort(loader.rects.begin(), loader.rects.end(), lambda);

        const auto totalArea = 39892.5;

        CHECK(maxDim == Approx(loader.getMaxDim()));
        CHECK(totalArea == Approx(loader.getTotalArea()));

        auto packer = BinPacker<BinItem, BinItem>(loader.getMaxDim() * MIN_LAYERS_PER_BIN);
        packer.pack(loader.rects);
        CHECK(packer.bins.size() == 19999);
    }

    SECTION("Integration test b: 160000 uniformly distributed rects")
    {

        RISSPacker pack = RISSPacker("uniform160000.txt", "result.txt");
        pack.run();

        std::string line;
        std::ifstream infile("result.txt");

        std::string x;
        float y = 0.0;

        std::getline(infile, line);
        std::istringstream iss(line);

        iss >> x >> y;
        CHECK(x == "SideLength=");
        CHECK(y == 200);
    }

    SECTION("Integration test a: 1600 uniformly distributed rects")
    {
        const auto* fileName = "uniform1600.txt";
        auto loader = ShapeSorter(fileName);
        loader.load(2);
        std::sort(loader.rects.begin(), loader.rects.end(), lambda);

        const auto totalArea = 410.455;

        CHECK(maxDim == Approx(loader.getMaxDim()));
        CHECK(totalArea == Approx(loader.getTotalArea()));

        auto packer = BinPacker<BinItem, BinItem>(loader.getMaxDim() * MIN_LAYERS_PER_BIN);
        packer.pack(loader.rects);
        CHECK(packer.bins.size() == 208);
    }

    SECTION("Integration test b: 1600 uniformly distributed rects")
    {

        RISSPacker pack = RISSPacker("uniform1600.txt", "result.txt");
        pack.run();

        std::string line;
        std::ifstream infile("result.txt");

        std::string x;
        float y = 0.0;

        std::getline(infile, line);
        std::istringstream iss(line);

        iss >> x >> y;
        CHECK(x == "SideLength=");
        CHECK(y == Approx(23.9995));
    }
}

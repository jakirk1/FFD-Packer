/*! \file ProblemFactory.hpp
* File containing the Problem Factory class.

* Copyright (c) 2020-2021 FFD-Packer contributors <https://gitlab.com/jakirk1/FFD-Packer>
* This file is part of the FFD-Packer project, released under the BSD 3-Clause License.
*/

#ifndef PROBLEMFACTORY_HPP
#define PROBLEMFACTORY_HPP

#include "RISSPacker.hpp"
#include "CuboidPacker.hpp"
#include "PackingProblem.hpp"
#include <map>
#include <memory>

template <typename T>
std::unique_ptr<PackingProblem> CreateProblemT(const std::string& rectFile, const std::string& resultFile)
{
    return std::make_unique<T>(rectFile, resultFile);
}

class ProblemFactory {
public:
    static std::unique_ptr<PackingProblem> CreatePacker(const std::string& problemName, const std::string& rectFile, const std::string& resultFile)
    {
        mapType fmap;
        ConstructMap(fmap);
        mapType::iterator iter = fmap.find(problemName);

        if (iter != fmap.end())
            return fmap[problemName](rectFile, resultFile);
        else {
            throw std::runtime_error("The PackingProblem specified: " + problemName + ", is not defined. Available problems:\n'RISS' : Rectangles Inside a Single Square.");
        }
    }

private:
    using mapType = std::map<std::string, std::unique_ptr<PackingProblem> (*)(const std::string& rectFile, const std::string& resultFile)>;

    static void ConstructMap(mapType& fmap)
    {
        fmap["RISS"] = &CreateProblemT<RISSPacker>;
        fmap["Cuboid"] = &CreateProblemT<CuboidPacker>;
    }
};
#endif
/*! \file Bin.hpp
* File containing the BinItem and Bin structs.

* Copyright (c) 2020-2021 FFD-Packer contributors <https://gitlab.com/jakirk1/FFD-Packer>
* This file is part of the FFD-Packer project, released under the BSD 3-Clause License.
*/

#ifndef BIN_HPP
#define BIN_HPP
#include <vector>

struct BinItem {

    std::vector<BinItem> items;
    std::vector<float> dims;
    float pos{ 0.0 };
};

struct Bin {

    std::vector<BinItem> items;
};
#endif
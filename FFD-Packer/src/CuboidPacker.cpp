// Copyright(c) 2020 - 2021 hyper-cube-fastpack contributors < https://gitlab.com/jakirk1/hyper-cube-fastpack>
// This file is part of the hyper-cube-fastpack project, released under the BSD 3 - Clause License.
#include "CuboidPacker.hpp"
#include <iostream>
#include <math.h>

CuboidPacker::CuboidPacker(const std::string& _rectFile, const std::string& _resultFile)
    : PackingProblem(_rectFile)
    , resultFile(_resultFile)
{
}

void CuboidPacker::run()
{
    try {
        loader.load(3);
    }
    catch (std::runtime_error const& e) {
        throw;
    }

    std::sort(loader.rects.begin(), loader.rects.end(), [](const BinItem& lhs, const BinItem& rhs) { return lhs.dims[2] > rhs.dims[2]; });

    const float MIN_SHELVES_PER_BIN = 3.0;

    float binDim = loader.getMaxDim() * MIN_SHELVES_PER_BIN;
    int totalBins = 0;
    const auto RECTS_PER_THREAD = 20000;
    auto nSplit = static_cast<int>(std::floor(loader.rects.size() / RECTS_PER_THREAD));

#ifdef _OPENMP
#pragma omp parallel default(none) \
    shared(totalBins, binDim, nSplit, RECTS_PER_THREAD, std::cout)
#endif
    {
#ifdef _OPENMP
#pragma omp for schedule(dynamic, 1) reduction(+ : totalBins)
#endif
        for (int j = 0; j < nSplit - 1; j++) {
            std::vector<BinItem> bin1;
#ifdef _OPENMP
#pragma omp critical
#endif
            {
                for (int i = 0; i < RECTS_PER_THREAD; i++) {
                    bin1.emplace_back(loader.rects.back());
                    loader.rects.pop_back();
                }
            }
            sorting(bin1, binDim, totalBins);
        }
    }

    std::vector<BinItem> bin1;

    while (loader.rects.size() > 0) {
        bin1.emplace_back(loader.rects.back());
        loader.rects.pop_back();
    }

    sorting(bin1, binDim, totalBins);

    auto n = static_cast<int>(floor(cbrt(totalBins)));
    auto nCb = pow(n, 3);
    auto extra = (totalBins - nCb);
    auto sideLength = binDim * static_cast<float>(n);

    if (extra > 0) {
        sideLength += binDim;
    }
    std::cout << "nBins= " << totalBins << "\n";
    std::cout << "CubeSideLength= " << sideLength << "\n";
    std::cout << "total volume of bins= " << totalBins * pow(binDim, 3) << "\n";
    std::cout << "total volume of cuboids = " << loader.getTotalArea() << "\n";
    std::cout << "Packing fraction = " << loader.getTotalArea() / (totalBins * pow(binDim, 3)) << "\n";
}

void CuboidPacker::sorting(std::vector<BinItem>& bin1, const float& binDim, int& totalBins)
{
    auto lambda = [](const BinItem& lhs, const BinItem& rhs) { return lhs.dims[1] > rhs.dims[1]; };
    std::sort(bin1.begin(), bin1.end(), lambda);

    auto packer = std::make_unique<BinPacker<BinItem, BinItem> >(binDim);
    packer->pack(bin1);

    for (auto& shelf : packer->bins) {
        shelf.dims.push_back(shelf.items[0].dims[1]);
    }

    for (auto& shelf : packer->bins) {

        float max = 0.0;

        for (auto& rect : shelf.items) {
            if (rect.dims[2] > max) {
                max = rect.dims[2];
            }
        }

        shelf.dims.emplace_back(std::move(max));
    }

    std::sort(packer->bins.begin(), packer->bins.end(), lambda);

    auto shelfPacker = std::make_unique<BinPacker<BinItem, BinItem> >(binDim);
    shelfPacker->pack(packer->bins);
    packer.reset();

    for (auto& shelves : shelfPacker->bins) {
        shelves.dims.push_back(shelves.items[0].dims[1]);
    }

    auto shelvesPacker = std::make_unique<BinPacker<BinItem, Bin> >(binDim);
    shelvesPacker->pack(shelfPacker->bins);
    shelfPacker.reset();

    totalBins += shelvesPacker->bins.size();

    shelvesPacker.reset();
}
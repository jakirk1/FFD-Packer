// Copyright(c) 2020 - 2021 FFD-Packer contributors < https://gitlab.com/jakirk1/FFD-Packer>
// This file is part of the FFD-Packer project, released under the BSD 3 - Clause License.

#include "Node.hpp"

Node::Node(const float& _gap, int&& _id, Node* _up, bool&& _leftFlag, bool _red)
    : gap(_gap)
    , id(std::move(_id))
    , up(_up)
    , leftFlag(std::move(_leftFlag))
    , red(std::move(_red))
{
}

Node::~Node()
{
    delete left;
    delete right;
}

/*! \file RISSPacker.hpp
* File containing the RISSPacker (Rectangles Into a Smallest Square Packer) class.

* Copyright (c) 2020-2021 FFD-Packer contributors <https://gitlab.com/jakirk1/FFD-Packer>
* This file is part of the FFD-Packer project, released under the BSD 3-Clause License.
*/

#ifndef RISSPACKER_HPP
#define RISSPACKER_HPP
#ifdef __linux__
#include <math.h>
#endif

#include "PackingProblem.hpp"
#include "Bin.hpp"
#include "BinPacker.hpp"
#include "ShapeSorter.hpp"
#include <fstream>
#include <iostream>
#include <algorithm>
#include <memory>

class RISSPacker : public PackingProblem {
public:
    RISSPacker(const std::string& _rectFile, const std::string& _resultFile);
    void run() override;

private:
    const std::string resultFile;

    void saveSquare(const std::vector<Bin>& sqBins, const float& binDim) const;
    void saveBin(const Bin& bin, float&& posX, float&& posY, std::ofstream& file) const;
};
#endif

/*
* Copyright (c) 2020-2021 FFD-Packer contributors <https://gitlab.com/jakirk1/FFD-Packer>
* This file is part of the FFD-Packer project, released under the BSD 3-Clause License.
*/

#include "RISSPacker.hpp"
#include "CuboidPacker.hpp"
#include "ProblemFactory.hpp"
#include <memory>

int main(int argc, char* argv[])
{
    if (argc < 3) {
        std::cout << "Not enough input arguments, minimum arguments required:-\n1: Problem type\n2: Input file name\n";
        exit(1);
    }

    std::string outputFile;

    if (argc == 3) {
        outputFile = "result.txt";
    }
    else {
        outputFile = std::string(argv[3]);
    }

    try {
        auto packer = ProblemFactory::CreatePacker(std::string(argv[1]), std::string(argv[2]), outputFile);
        packer->run();
    }
    catch (std::runtime_error const& e) {
        std::string error = std::string(e.what());
        std::cout << "Critical errors encountered: \n \n"
                  << error << "\nProgram terminating\n";
        exit(1);
    }

    return 0;
};
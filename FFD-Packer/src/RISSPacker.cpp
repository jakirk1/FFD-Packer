// Copyright(c) 2020 - 2021 FFD-Packer contributors < https://gitlab.com/jakirk1/FFD-Packer>
// This file is part of the FFD-Packer project, released under the BSD 3 - Clause License.
#include "RISSPacker.hpp"
#include <iostream>

RISSPacker::RISSPacker(const std::string& _rectFile, const std::string& _resultFile)
    : PackingProblem(_rectFile)
    , resultFile(_resultFile)
{
}

void RISSPacker::run()
{
    try {
        loader.load(2);
    }
    catch (std::runtime_error const& e) {
        throw;
    }

    std::sort(loader.rects.begin(), loader.rects.end(), [](const BinItem& lhs, const BinItem& rhs) { return lhs.dims[1] > rhs.dims[1]; });

    const float MIN_SHELVES_PER_BIN = 4.0;
    float binDim = loader.getMaxDim() * MIN_SHELVES_PER_BIN;

    ///////////////////////////
    // Pack first dimension  //
    ///////////////////////////

    auto packer = std::make_unique<BinPacker<BinItem, BinItem> >(binDim);
    packer->pack(loader.rects);
    loader.rects.clear();

    for (auto& shelf : packer->bins) {
        if (!shelf.items.empty()) {
            shelf.dims.emplace_back(shelf.items[0].dims[1]);
        }
        else {
            shelf.dims.emplace_back(0);
        }
    }

    ///////////////////////////
    // Pack second dimension //
    ///////////////////////////

    auto shelfPacker = BinPacker<BinItem, Bin>(binDim);
    shelfPacker.pack(packer->bins);
    packer.reset();

    ///
    try {
        saveSquare(shelfPacker.bins, binDim);
    }
    catch (std::runtime_error const& e) {
        throw;
    }
}

void RISSPacker::saveSquare(const std::vector<Bin>& sqBins, const float& binDim) const
{
    std::ofstream fp(resultFile);
    if (fp.is_open()) {

        auto n = static_cast<int>(floor(sqrt(sqBins.size())));
        auto nSq = n * n;
        auto extra = static_cast<int>(sqBins.size()) - nSq;
        auto sideLength = binDim * static_cast<float>(n);

        if (extra > 0) {
            sideLength += binDim;
        }

        fp << "SideLength= " << sideLength << "\n";
        fp << "total area of rectangles = " << loader.getTotalArea() << "\n";
        fp << "bin side length = " << binDim << "\n";
        fp << "total number of bins = " << sqBins.size() << "\n";

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                auto ID = n * i + j;
                saveBin(sqBins[ID], static_cast<float>(i) * binDim, static_cast<float>(j) * binDim, fp);
            }
        }

        auto l = 0;

        if (extra % 2) {

            l = static_cast<int>(floor((extra / 2)));
        }
        else {
            l = extra / 2;
        }

        //cover top of square
        for (int i = nSq; i < nSq + l; i++) {
            saveBin(sqBins[i], static_cast<float>(i - nSq) * binDim, static_cast<float>(n) * binDim, fp);
        }

        //cover side of square
        for (auto i = nSq + l; i < nSq + extra; i++) {
            saveBin(sqBins[i], static_cast<float>(n) * binDim, static_cast<float>(i - (nSq + l)) * binDim, fp);
        }

        fp.close();
    }
    else {
        throw std::runtime_error("Error opening file: " + resultFile);
    }
}

void RISSPacker::saveBin(const Bin& bin, float&& posX, float&& posY, std::ofstream& file) const
{
    for (const auto& shelf : bin.items) {
        for (const auto& rect : shelf.items) {
            file << rect.dims[0] << " " << rect.dims[1] << " " << rect.pos + posX << " " << shelf.pos + posY << "\n";
        }
    }
}
/*! \file ShapeSorter.hpp
* File containing the ShapeSorter class that loads rectangles from a file and sorts them in a std::vector.

* Copyright (c) 2020-2021 FFD-Packer contributors <https://gitlab.com/jakirk1/FFD-Packer>
* This file is part of the FFD-Packer project, released under the BSD 3-Clause License.
*/

#ifndef SHAPESORTER_HPP
#define SHAPESORTER_HPP
#include "Bin.hpp"
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <cstring>
#include <iterator>

class ShapeSorter {

public:
    ShapeSorter(const std::string& _file);
    void load(int nDims);
    float getMaxDim() const;
    float getTotalArea() const;

    std::vector<BinItem> rects;

private:
    const std::string file;
    float totalArea;
    float maxDim;
};
#endif
/*! \file CuboidPacker.hpp
* File containing the CuboidPacker class.

* Copyright (c) 2020-2021 hyper-cube-fastpack contributors <https://gitlab.com/jakirk1/hyper-cube-fastpack>
* This file is part of the hyper-cube-fastpack project, released under the BSD 3-Clause License.
*/

#ifndef CUBOIDPACKER_HPP
#define CUBOIDPACKER_HPP
#ifdef __linux__
#include <math.h>
#endif
#include "PackingProblem.hpp"
#include "Bin.hpp"
#include "ShapeSorter.hpp"
#include "BinPacker.hpp"

#include <fstream>
#include <iostream>
#include <algorithm>
#include <memory>
#ifdef _OPENMP
#include <omp.h>
#endif

class CuboidPacker : public PackingProblem {
public:
    CuboidPacker(const std::string& _rectFile, const std::string& _resultFile);
    void run() override;

private:
    const std::string resultFile;
    void sorting(std::vector<BinItem>& bin1, const float& binDim, int& totalBins);
};
#endif

/*! \file binDims.hpp
* File containing the binDims struct.

* Copyright (c) 2020-2021 FFD-Packer contributors <https://gitlab.com/jakirk1/FFD-Packer>
* This file is part of the FFD-Packer project, released under the BSD 3-Clause License.
*/

#ifndef BINDIMS_HPP
#define BINDIMS_HPP

struct BinDims {

    const float x;
    const float y;

    BinDims(const float& _x, const float& _y)
        : x(_x)
        , y(_y)
    {
    }
};
#endif
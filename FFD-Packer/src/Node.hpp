/*! \file Node.hpp
* File containing the Node class for the Red-Black Johnson tree used by BinPacker.

* Copyright (c) 2020-2021 FFD-Packer contributors <https://gitlab.com/jakirk1/FFD-Packer>
* This file is part of the FFD-Packer project, released under the BSD 3-Clause License.
*/

#ifndef NODE_HPP
#define NODE_HPP
#include <utility>

struct Node {

public:
    Node(const float& _gap, int&& _id, Node* _up, bool&& _leftFlag, bool _red);

    ~Node();

    Node* left{ nullptr };
    Node* right{ nullptr };
    Node* up;

    bool leftFlag;
    bool red;

    int id;
    float gap;
};
#endif
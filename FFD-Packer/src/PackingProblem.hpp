/*! \file PackingProblem.hpp
* File containing the PackingProblem abstract class.

* Copyright (c) 2020-2021 FFD-Packer contributors <https://gitlab.com/jakirk1/FFD-Packer>
* This file is part of the FFD-Packer project, released under the BSD 3-Clause License.
*/

#ifndef PACKINGPROBLEM_HPP
#define PACKINGPROBLEM_HPP

#include "ShapeSorter.hpp"

class PackingProblem {
public:
    PackingProblem(const std::string& _rectFile)
        : loader(ShapeSorter(_rectFile)){};
    virtual ~PackingProblem() = default;
    virtual void run() = 0;

    ShapeSorter loader;
};
#endif

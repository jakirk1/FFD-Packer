// Copyright(c) 2020 - 2021 FFD-Packer contributors < https://gitlab.com/jakirk1/FFD-Packer>
// This file is part of the FFD-Packer project, released under the BSD 3 - Clause License.
#include "ShapeSorter.hpp"

ShapeSorter::ShapeSorter(const std::string& _file)
    : file(_file)
    , totalArea(0.0)
    , maxDim(0.0)
{
}

void ShapeSorter::load(int nDims)
{
    std::string line;
    std::ifstream infile(file);

    if (infile.is_open()) {
        while (std::getline(infile, line)) {
            std::istringstream iss(line);
            if (std::distance(std::istream_iterator<std::string>(iss),
                    std::istream_iterator<std::string>())
                != nDims) {
                throw std::runtime_error("The file named " + file + " contains an invalid line:\n" + line + "\nEach line must only consist of " + std::to_string(nDims) + " space separated rectangle dimensions.");
            }
        }

        infile.clear();
        infile.seekg(0, infile.beg);
        auto x = -1.0;
        while (std::getline(infile, line)) {
            std::istringstream iss(line);
            auto rect = BinItem();
            auto area = 1.0;
            for (auto i = 0; i < nDims; i++) {
                iss >> x;
                if (x < 0) {
                    throw std::runtime_error("The file named " + file + " contains an invalid line:\n" + line + "\nEach rectangle dimension must be greater than zero.");
                }
                if (x > maxDim) {
                    maxDim = x;
                }
                area *= x;
                rect.dims.emplace_back(x);
            }
            rects.emplace_back(rect);
            totalArea += area;
        }
        infile.close();
    }
    else {
        throw std::runtime_error("Error opening file: " + file);
    }
}

float ShapeSorter::getMaxDim() const
{
    return maxDim;
}

float ShapeSorter::getTotalArea() const
{
    return totalArea;
}

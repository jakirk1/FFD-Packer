import vpython
from vpython import *

with open("result.txt") as fp:
    for i, line in enumerate(fp):

        nums = line.split()
        
        nums[0] = float(nums[0])
        nums[1] = float(nums[1])
        nums[2] = float(nums[2]) 
        nums[3] = float(nums[3])
        
        cylinder(pos=vector(nums[2], nums[3], 0), axis=vector(nums[0], 0, 0), radius=0.01,color=color.blue)
        cylinder(pos=vector(nums[2], nums[3], 0), axis=vector(0, nums[1], 0), radius=0.01,color=color.blue)
        cylinder(pos=vector(nums[2] + nums[0], nums[3], 0), axis=vector(0, nums[1], 0), radius=0.01,color=color.blue)
        cylinder(pos=vector(nums[2] , nums[3] + nums[1], 0), axis=vector(nums[0], 0, 0), radius=0.01,color=color.blue)

